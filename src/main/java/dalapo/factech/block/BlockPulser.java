package dalapo.factech.block;

import javax.annotation.Nonnull;

import dalapo.factech.FactoryTech;
import dalapo.factech.helper.FacTileHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.reference.AABBList;
import dalapo.factech.reference.StateList;
import dalapo.factech.tileentity.automation.TileEntityPulser;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockPulser extends BlockDirectionalTile
{
	private int guiID;
	public BlockPulser(Material materialIn, String name, String teid, int guiID, boolean locked)
	{
		super(materialIn, name, teid, locked);
		this.guiID = guiID;
		setDefaultState(blockState.getBaseState().withProperty(StateList.powered, false));
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isBlockNormalCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean canProvidePower(IBlockState state)
    {
        return true;
    }
	
	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess world, BlockPos pos)
	{
		return AABBList.FLAT;
	}
	
	@Override
	public AxisAlignedBB getCollisionBoundingBox(IBlockState state, IBlockAccess world, BlockPos pos)
	{
		return AABBList.FLAT;
	}
	
	@Nonnull
	@Override
	public BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, StateList.directions, PART_ID, StateList.powered);
	}
	
	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer ep, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		super.onBlockActivated(world, pos, state, ep, hand, side, hitX, hitY, hitZ);
		ep.openGui(FactoryTech.instance, guiID, world, pos.getX(), pos.getY(), pos.getZ());
		return true;
	}
	
	@Override
	public int getWeakPower(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing side)
	{
		if (side == state.getValue(StateList.directions))
		{
			return state.getValue(StateList.powered) ? 15 : 0;
		}
		return 0;
	}
}