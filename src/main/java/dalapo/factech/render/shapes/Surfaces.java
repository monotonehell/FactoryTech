package dalapo.factech.render.shapes;

import net.minecraft.util.math.Vec3d;
import static net.minecraft.util.math.MathHelper.*;

import net.minecraft.util.math.Vec2f;

public class Surfaces
{
	private Surfaces() {}
	
	public static Vec3d torus(Vec3d uv, float...constants)
	{
		float a;
		float b;
		if (constants.length == 0)
		{
			a = 1;
			b = 1;
		}
		if (constants.length == 1)
		{
			a = constants[0];
			b = 1;
		}
		else
		{
			a = (float)constants[0];
			b = (float)constants[1];
		}
		float u = (float)uv.x;
		float v = (float)uv.y;
		return new Vec3d((a + b*cos(u))*cos(v), b*sin(u), (a + b*cos(u))*sin(v));
	}
}