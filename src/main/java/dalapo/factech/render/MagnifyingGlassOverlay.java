package dalapo.factech.render;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import dalapo.factech.helper.FacEntityHelper;
import dalapo.factech.helper.FacGuiHelper;
import dalapo.factech.helper.FacRenderHelper;
import dalapo.factech.helper.FacRenderHelper.Point;
import dalapo.factech.helper.Pair;
import dalapo.factech.init.ItemRegistry;
import dalapo.factech.tileentity.IMagnifyingGlassInfo;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

@SideOnly(Side.CLIENT)
public class MagnifyingGlassOverlay {
	private MagnifyingGlassOverlay() {}
	
	public static final MagnifyingGlassOverlay instance = new MagnifyingGlassOverlay();
	
	@SubscribeEvent(priority = EventPriority.HIGHEST, receiveCanceled = true)
	public void renderOverlayEvent(RenderGameOverlayEvent.Post evt)
	{
		int scaledX = evt.getResolution().getScaledWidth();
		int scaledY = evt.getResolution().getScaledHeight();
		Minecraft mc = Minecraft.getMinecraft();
		int width = mc.displayWidth;
		int height = mc.displayHeight;
		EntityPlayerSP player = mc.player;
		if (!player.isSneaking() && FacEntityHelper.isHoldingItem(player, ItemRegistry.magnifyingGlass))
		{
			GlStateManager.pushMatrix();
			FacGuiHelper.bindTex("magnifying_glass_overlay");
			GlStateManager.enableAlpha();
			GlStateManager.enableBlend();
			GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			GlStateManager.color(1.0F, 1.0F, 1.0F, 0.95F);
			FacGuiHelper.drawTexturedModalRect(scaledX/2 - 128, scaledY/2 - 128, 10, 0, 0, 256, 256);
			GlStateManager.disableAlpha();
			RayTraceResult mouseOver = mc.objectMouseOver;
			if (mouseOver != null && mouseOver.typeOfHit == RayTraceResult.Type.BLOCK)
			{
				TileEntity te = player.getEntityWorld().getTileEntity(mouseOver.getBlockPos());
				if (te instanceof IMagnifyingGlassInfo)
				{
					List<String> info = ((IMagnifyingGlassInfo)te).getGlassInfo();
					GlStateManager.translate(0, 0, 11);
					for (int i=0; i<info.size(); i++)
					{
						mc.fontRenderer.drawString(info.get(i), scaledX/2 - 80, scaledY/2 - 40 + i*16, 0x040404);
					}
					
				}
			}
			GlStateManager.popMatrix();
		}
	}
}