package dalapo.factech.render.tesr;

import java.util.Deque;

import org.lwjgl.opengl.GL11;

import dalapo.factech.block.BlockDirectional;
import dalapo.factech.helper.FacRenderHelper;
import dalapo.factech.helper.FacTesrHelper;
import dalapo.factech.tileentity.automation.TileEntityElevator;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

@SideOnly(Side.CLIENT)
public class TesrElevator extends TesrOmnidirectional<TileEntityElevator>
{
	static final TESRELEV auth = new TESRELEV();
	
	public void doRender(TileEntityElevator te, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
    {
        Deque<ItemStack> stacks = te.getStacks(auth);
        ItemStack andOneMore = te.getLegacy();
        GlStateManager.pushMatrix();
//      GlStateManager.translate(x, y, z);
        int i = 0;
        for (ItemStack stack : stacks)
        {
        	if (!stack.isEmpty())
        	{
        		GlStateManager.pushMatrix();
        		GlStateManager.translate(0, 0.5 - (i - partialTicks) / 20, 0.0625);
        		GlStateManager.scale(0.25, 0.25, 0.25);
        		FacTesrHelper.renderStack(stack);
        		GlStateManager.popMatrix();
        	}
        	i++;
        }
        GlStateManager.pushMatrix();
		GlStateManager.translate(0, 0.5 + (partialTicks / 20), 0.0625);
		GlStateManager.scale(0.25, 0.25, 0.25);
		FacTesrHelper.renderStack(andOneMore);
		GlStateManager.popMatrix();
        GlStateManager.popMatrix();
        
        FacRenderHelper.bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		if (Minecraft.isAmbientOcclusionEnabled())
		{
			GlStateManager.shadeModel(GL11.GL_SMOOTH);
		}
		else
		{
			GlStateManager.shadeModel(GL11.GL_FLAT);
		}
		World world = te.getWorld();
		GlStateManager.pushMatrix();
		GlStateManager.translate(-te.getPos().getX()-0.5, -te.getPos().getY()-0.5, -te.getPos().getZ()-0.938);
		Tessellator v5 = Tessellator.getInstance();
		BufferBuilder builder = v5.getBuffer();
		IBlockState state = te.getBlockType().getDefaultState().withProperty(BlockDirectional.PART_ID, 1);
		BlockRendererDispatcher dispatcher = Minecraft.getMinecraft().getBlockRendererDispatcher();
		for (int j=0; j<4; j++)
		{
			GlStateManager.pushMatrix();
			IBakedModel model = dispatcher.getModelForState(state);
			builder.begin(GL11.GL_QUADS, DefaultVertexFormats.BLOCK);
			long height = System.currentTimeMillis() / 16 % 16;
			if (!Minecraft.getMinecraft().isGamePaused()) GlStateManager.translate(0, (j/4.0) + (height / 64.0), 0);
//			GlStateManager.translate(0.0625, 0, 0);
//			GlStateManager.pushMatrix();
			dispatcher.getBlockModelRenderer().renderModel(world, model, state, te.getPos(), builder, false);	
			v5.draw();
//			GlStateManager.popMatrix();
			GlStateManager.popMatrix();
		}
		GlStateManager.popMatrix();
    }
	
	public static final class TESRELEV { private TESRELEV() {}}
}