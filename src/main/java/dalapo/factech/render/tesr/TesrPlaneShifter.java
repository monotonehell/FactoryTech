package dalapo.factech.render.tesr;

import org.lwjgl.opengl.GL11;

import dalapo.factech.render.shapes.Torus;
import dalapo.factech.tileentity.automation.TileEntityPlaneShifter;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

@SideOnly(Side.CLIENT)
public class TesrPlaneShifter extends TileEntitySpecialRenderer<TileEntityPlaneShifter>
{
	Torus torus = new Torus(0.25F, 0.1F);
	
	public TesrPlaneShifter()
	{
		setLightmapDisabled(true);
	}
	
	@Override
	public void render(TileEntityPlaneShifter te, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
	{
		GlStateManager.pushMatrix();
		GlStateManager.disableTexture2D();
		GlStateManager.disableLighting();
		GlStateManager.shadeModel(GL11.GL_FLAT);
		GlStateManager.disableBlend();
		GlStateManager.enableAlpha();
		GlStateManager.translate(x + 0.5, y + 0.5 + 0.1*MathHelper.sin(te.ticks + (partialTicks*te.dt)), z + 0.5);
		GlStateManager.rotate(8 * (te.ticks + (partialTicks*te.dt)), 0, 1, 0);
		torus.render(0, 0.2F, 0.4F);
		GlStateManager.enableBlend();
		GlStateManager.shadeModel(GL11.GL_SMOOTH);
		GlStateManager.color(1F, 1F, 1F, 1F);
		GlStateManager.enableLighting();
		GlStateManager.disableAlpha();
		GlStateManager.enableTexture2D();
		GlStateManager.popMatrix();
	}
}