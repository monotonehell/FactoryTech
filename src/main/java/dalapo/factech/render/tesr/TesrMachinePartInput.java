package dalapo.factech.render.tesr;

import org.lwjgl.opengl.GL11;

import dalapo.factech.helper.FacEntityHelper;
import dalapo.factech.helper.FacRenderHelper;
import dalapo.factech.init.ItemRegistry;
import dalapo.factech.tileentity.TileEntityMachine;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumFacing;

import static dalapo.factech.helper.FacRenderHelper.Point;

public class TesrMachinePartInput extends TesrMachine<TileEntityMachine>
{

	public TesrMachinePartInput(boolean directional) {
		super(false);
	}

	@Override
	public void doRender(TileEntityMachine te, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
	{
		Minecraft mc = Minecraft.getMinecraft();
		if (FacEntityHelper.isHoldingItem(mc.player, ItemRegistry.magnifyingGlass) && mc.objectMouseOver != null && mc.objectMouseOver.getBlockPos() != null && mc.objectMouseOver.getBlockPos().equals(te.getPos()))
		{
			GlStateManager.pushMatrix();
			GlStateManager.disableTexture2D();
			GlStateManager.disableLighting();
			GlStateManager.shadeModel(GL11.GL_FLAT);
			GlStateManager.disableBlend();
			GlStateManager.enableAlpha();
			GlStateManager.translate(-0.5, -0.5, -0.5);
			GlStateManager.glLineWidth(2.0F);
			EnumFacing[] inputs = te.getTruePartSides();
			for (EnumFacing side : inputs)
			{
				GlStateManager.pushMatrix();
				Tessellator v5 = Tessellator.getInstance();
				BufferBuilder buffer = v5.getBuffer();
				GlStateManager.translate(side.getFrontOffsetX(), side.getFrontOffsetY(), side.getFrontOffsetZ());
				GlStateManager.color(0.0F, 1.0F, 0.0F);
				buffer.begin(GL11.GL_LINES, DefaultVertexFormats.POSITION);
				Point[] points = new Point[8];
				for (int i=0; i<8; i++)
				{
					points[i] = new Point(i & 1, (i >> 1) & 1, (i >> 2) & 1);
				}
				for (int i=0; i<8; i++)
				{
					for (int j=0; j<i; j++)
					{
						boolean doDraw = false;
						for (int p=0; p<3; p++)
						{
							if (points[i].getField(p) == points[j].getField(p)) doDraw = true;
						}
						if (doDraw) FacRenderHelper.drawLine(buffer, points[i], points[j]);
					}
				}
				v5.draw();
				GlStateManager.popMatrix();
			}
			GlStateManager.enableBlend();
			GlStateManager.shadeModel(GL11.GL_SMOOTH);
			GlStateManager.color(1F, 1F, 1F, 1F);
			GlStateManager.enableLighting();
			GlStateManager.disableAlpha();
			GlStateManager.enableTexture2D();
			GlStateManager.popMatrix();
		}
	}
}