package dalapo.factech.helper;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class FacEntityHelper
{
	private FacEntityHelper() {}
	
	public static void stopEntity(Entity entity)
	{
		entity.motionX = 0;
		entity.motionY = 0;
		entity.motionZ = 0;
	}
	
	public static boolean isHoldingItem(EntityPlayer ep, Item item)
	{
		return ep.getHeldItemMainhand().getItem() == item || ep.getHeldItemOffhand().getItem() == item;
	}
}