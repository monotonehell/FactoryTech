package dalapo.factech.gui;

import dalapo.factech.helper.Logger;
import dalapo.factech.tileentity.TileEntityBase;
import dalapo.factech.tileentity.TileEntityBasicInventory;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class ContainerEmpty extends ContainerBase
{
	protected TileEntityBase tile;
	public ContainerEmpty(TileEntityBase te)
	{
		super(te);
	}

	@Override
	public void addListener(IContainerListener listener)
	{
		super.addListener(listener);
	}
	
	@Override
	public ItemStack slotClick(int slotId, int dragType, ClickType clicktype, EntityPlayer ep)
	{
		return ItemStack.EMPTY;
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer playerIn)
	{
		return true;
	}
}