package dalapo.factech.gui.widget;

import dalapo.factech.gui.GuiFacInventory;
import dalapo.factech.gui.GuiTileEntity;
import net.minecraft.client.gui.Gui;
import net.minecraft.inventory.Container;

public abstract class FacTechWidget extends Gui
{
	protected int x;
	protected int y;
	protected int width;
	protected int height;
	protected GuiTileEntity parent;
	
	public FacTechWidget(GuiTileEntity parent, int x, int y, int w, int h)
	{
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;
		this.parent = parent;
	}
	
	public abstract void init();
	public abstract void handle(int mouseX, int mouseY, int mouseButton, boolean shift);
	
	public void setZLevel(int z)
	{
		zLevel = z;
	}
	
	public int getX()
	{
		return x;
	}
	public int getY()
	{
		return y;
	}
	public int getWidth()
	{
		return width;
	}
	public int getHeight()
	{
		return height;
	}
	protected GuiTileEntity getParent()
	{
		return parent;
	}
	
	public boolean isPointInBounds(int x, int y)
	{
		return x >= this.x && x <= this.x + this.width && y >= this.y && y <= this.y + this.height;
	}
	public abstract String getTooltip();
	public abstract void draw(int guiLeft, int guiTop);
}