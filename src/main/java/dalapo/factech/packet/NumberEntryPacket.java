package dalapo.factech.packet;

import dalapo.factech.gui.ContainerBase;
import dalapo.factech.tileentity.TileEntityBase;
import dalapo.factech.tileentity.TileEntityBasicInventory;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class NumberEntryPacket extends FacTechPacket
{
	byte id;
	int num;
	
	public NumberEntryPacket(byte id, int num)
	{
		this.id = id;
		this.num = num;
	}
	public NumberEntryPacket() {}
	
	@Override
	protected void actuallyDoHandle(FacTechPacket msg, World world, EntityPlayer ep, boolean isClient) {
		ContainerBase gui = (ContainerBase)ep.openContainer;
		NumberEntryPacket packet = (NumberEntryPacket)msg;
		TileEntityBase te = gui.getTile();
		te.setField(packet.id, packet.num);
		gui.detectAndSendChanges();
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		id = buf.readByte();
		num = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeByte(id);
		buf.writeInt(num);
	}

}
