package dalapo.factech.packet;

import dalapo.factech.gui.ContainerBase;
import dalapo.factech.helper.Logger;
import dalapo.factech.tileentity.specialized.TileEntityCircuitScribe;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class CircuitScribePacket extends FacTechPacket {

	byte pattern;
	
	public CircuitScribePacket(TileEntityCircuitScribe te)
	{
		pattern = (byte)te.getPattern();
	}
	
	public CircuitScribePacket() {}
	
	@Override
	protected void actuallyDoHandle(FacTechPacket msg, World world, EntityPlayer ep, boolean isClient) {
		ContainerBase container = (ContainerBase)ep.openContainer;
		TileEntityCircuitScribe te = (TileEntityCircuitScribe)container.getTile();
		te.setPattern(((CircuitScribePacket)msg).pattern);
		container.detectAndSendChanges();
		te.getHasWork();
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		pattern = buf.readByte();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeByte(pattern);
	}
}