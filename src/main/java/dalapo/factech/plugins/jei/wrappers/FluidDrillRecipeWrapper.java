package dalapo.factech.plugins.jei.wrappers;

import java.util.List;

import dalapo.factech.helper.FacFluidRenderHelper;
import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.plugins.jei.BaseRecipeWrapper;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fluids.FluidStack;

public class FluidDrillRecipeWrapper extends BaseRecipeWrapper
{
	private FluidStack output;
	
	public FluidDrillRecipeWrapper(IGuiHelper helper, FluidStack out)
	{
		output = out;
	}
	
	@Override
	public void getIngredients(IIngredients ingredients)
	{
		ingredients.setOutput(FluidStack.class, output);
	}
	
	@Override
	public void drawInfo(Minecraft minecraft, int recipeWidth,
			int recipeHeight, int mouseX, int mouseY) {
		FacFluidRenderHelper.drawFluid(output, 88, 56, 16, 3);
	}
	
	@Override
	public List<String> getTooltipStrings(int mouseX, int mouseY) {
		List<String> tooltips = super.getTooltipStrings(mouseX, mouseY);
		if (FacMathHelper.isInRange(mouseY, 10, 58) && FacMathHelper.isInRange(mouseX, 88, 104) && output != null)
		{
			tooltips.add(output.getLocalizedName());
		}
		return tooltips;
	}
}