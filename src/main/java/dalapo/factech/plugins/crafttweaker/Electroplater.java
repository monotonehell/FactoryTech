package dalapo.factech.plugins.crafttweaker;

import java.util.Map.Entry;

import javax.annotation.Nullable;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import crafttweaker.CraftTweakerAPI;
import crafttweaker.IAction;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import dalapo.factech.auxiliary.MachineRecipes;
import dalapo.factech.auxiliary.MachineRecipes.MachineRecipe;
import dalapo.factech.helper.FacCraftTweakerHelper;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.Pair;

import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenClass("mods.factorytech.Electroplater")
@ZenRegister
public class Electroplater
{
	@ZenMethod
	public static void addRecipe(IItemStack output, IIngredient input)
	{
		for (IItemStack in : input.getItems())
		{
			CraftTweakerAPI.apply(new Add((ItemStack)in.getInternal(), (ItemStack)output.getInternal()));
		}
	}
	
	@ZenMethod
	public static void removeRecipe(IIngredient output)
	{
		for (IItemStack out : output.getItems())
		{
			CraftTweakerAPI.apply(new Remove(FacCraftTweakerHelper.toStack(out)));
		}
	}
	
	private static class Add implements IAction
	{
		private ItemStack in;
		private ItemStack out;
		
		public Add(ItemStack in, ItemStack out)
		{
			this.in = in;
			this.out = out;
		}
		
		@Override
		public void apply() {
			MachineRecipes.ELECTROPLATER.add(new MachineRecipe<ItemStack, ItemStack>(in, out, false));
		}

		@Override
		public String describe() {
			// TODO Auto-generated method stub
			return "Adding Electroplater recipe for " + in + " -> " + out;
		}
	}
	
	private static class Remove implements IAction
	{
		ItemStack output;
		
		public Remove(ItemStack o)
		{
			this.output = o;
		}
		@Override
		public void apply()
		{
			ScheduledRemovals.scheduleRemoval(MachineRecipes.ELECTROPLATER, output);
		}

		@Override
		public String describe() {
			return "Removing Electroplater recipe for " + output;
		}
		
	}
}