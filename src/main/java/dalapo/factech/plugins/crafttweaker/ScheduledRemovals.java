package dalapo.factech.plugins.crafttweaker;

import java.util.ArrayList;
import java.util.List;

import dalapo.factech.auxiliary.IMachineRecipe;
import dalapo.factech.auxiliary.MachineRecipes.MachineRecipe;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.Pair;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidStack;

public class ScheduledRemovals
{
	private static List<Pair<List, ItemStack>> itemRemovals = new ArrayList<>();
	private static List<Pair<List, FluidStack>> fluidRemovals = new ArrayList<>();
	
	public static void scheduleRemoval(List<? extends IMachineRecipe> list, ItemStack is)
	{
		itemRemovals.add(new Pair<>(list, is));
	}
	
	public static void scheduleRemoval(List<? extends IMachineRecipe> list, FluidStack fs)
	{
		fluidRemovals.add(new Pair<>(list, fs));
	}
	
	public static void removeAll()
	{
		for (Pair<List, ItemStack> p : itemRemovals)
		{
			for (int i=0; i<p.a.size(); i++)
			{
				IMachineRecipe<ItemStack> recipe = (IMachineRecipe)p.a.get(i);
				if (FacStackHelper.matchStacksWithWildcard(recipe.getOutputStack(), p.b))
				{
					p.a.set(i, null);
				}
			}

			itemRemovals.forEach(pair -> {
				pair.a.removeIf(r -> r == null);
			});
		}
		for (Pair<List, FluidStack> p : fluidRemovals)
		{
			for (int i=0; i<p.a.size(); i++)
			{
				IMachineRecipe<FluidStack> recipe = (IMachineRecipe)p.a.get(i);
				if (FacStackHelper.areFluidStacksIdentical(recipe.getOutputStack(), p.b))
				{
					p.a.set(i, null);
				}
			}
			fluidRemovals.forEach(pair -> {
				pair.a.removeIf(r -> r == null);
			});
		}
		
	}
}