package dalapo.factech.plugins.crafttweaker;

import java.util.Map.Entry;

import javax.annotation.Nullable;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import crafttweaker.CraftTweakerAPI;
import crafttweaker.IAction;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import crafttweaker.api.liquid.ILiquidStack;
import dalapo.factech.auxiliary.MachineRecipes;
import dalapo.factech.auxiliary.MachineRecipes.MachineRecipe;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.Pair;

import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenClass("mods.factorytech.Crucible")
@ZenRegister
public class Crucible
{
	@ZenMethod
	public static void addRecipe(ILiquidStack output, IIngredient inputItem, boolean worksWithBad)
	{
		for (IItemStack input : inputItem.getItems())
		{
			CraftTweakerAPI.apply(new Add((ItemStack)input.getInternal(), (FluidStack)output.getInternal(), false));
		}
	}
	
	@ZenMethod
	public static void removeRecipe(IIngredient output)
	{
		for (ILiquidStack liquid : output.getLiquids())
		{
			CraftTweakerAPI.apply(new Remove((FluidStack)output.getInternal()));
		}
	}
	
	private static class Add extends MasterAdd<ItemStack, FluidStack>
	{
		private ItemStack in;
		private FluidStack out;
		
		public Add(ItemStack in, FluidStack out, boolean worksWithBad)
		{
			super(in, out, worksWithBad, MachineRecipes.CRUCIBLE);
		}

		@Override
		public String describe()
		{
			return "Adding Crucible recipe for " + in + " -> " + out;
		}
	}
	
	private static class Remove implements IAction
	{
		FluidStack output;
		
		public Remove(FluidStack o)
		{
			this.output = o;
		}
		@Override
		public void apply()
		{
			ScheduledRemovals.scheduleRemoval(MachineRecipes.CRUCIBLE, output);
			/*
			for (int i=MachineRecipes.CRUCIBLE.size()-1; i>=0; i--)
			{
				if (FacStackHelper.areFluidStacksIdentical(MachineRecipes.CRUCIBLE.get(i).output(), output))
				{
					MachineRecipes.CRUCIBLE.remove(i);
				}
			}
			*/
		}

		@Override
		public String describe() {
			return "Removing Crucible recipe for " + output;
		}
		
	}
}