package dalapo.factech.auxiliary;

public interface IMachineRecipe<T> {
	public T getOutputStack();
}