package dalapo.factech.tileentity.specialized;

import java.util.ArrayList;
import java.util.List;

import dalapo.factech.FactoryTech;
import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.init.ItemRegistry;
import dalapo.factech.reference.PartList;
import dalapo.factech.reference.StateList;
import dalapo.factech.tileentity.TileEntityBasicInventory;
import dalapo.factech.tileentity.TileEntityMachine;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import net.minecraftforge.fml.relauncher.ReflectionHelper.UnableToFindClassException;

public class TileEntityEnergizer extends TileEntityBasicInventory implements ITickable
{
	public static final List<Class<? extends TileEntity>> BLACKLIST = new ArrayList<>();
	public static final int EXTRA_TICKS = 1;
	
	private boolean hasCore;
	private int age;
	
	static {
		blacklistTileEntity("dalapo.factech.tileentity.specialized.TileEntityEnergizer");
	}
	
	public TileEntityEnergizer()
	{
		super("energizer", 1);
		setDisplayName("External Overclock");
	}

	public static boolean blacklistTileEntity(String classname)
	{
		Class te;
		try {
			te = ReflectionHelper.getClass(FactoryTech.class.getClassLoader(), classname);
		}
		catch (UnableToFindClassException e)
		{
			return false;
		}
		
		if (TileEntity.class.isAssignableFrom(te))
		{
			BLACKLIST.add(te);
			return true;
		}
		return false;
	}
	
	public boolean isCharging()
	{
		return hasCore;
	}
	
	@Override
	public void onInventoryChanged(int slot)
	{
		if (!hasCore && getStackInSlot(slot).isItemEqual(new ItemStack(ItemRegistry.machinePart, 1, PartList.CORE.getFloor())))
		{
			getStackInSlot(slot).shrink(1);
			hasCore = true;
		}
	}
	
	@Override
	public void update()
	{
		if (hasCore)
		{
			for (EnumFacing dir : EnumFacing.VALUES)
			{
				TileEntity te = world.getTileEntity(FacMathHelper.withOffset(pos, dir));
				if (te instanceof ITickable && !(BLACKLIST.contains(te.getClass())))
				{
					for (int i=0; i<EXTRA_TICKS; i++)
					{
						((ITickable)te).update(); // I STEAL YOUR CODE, DRAGON
					}
				}
			}
			if (++age >= 600)
			{
				age = 0;
				if (getStackInSlot(0).isEmpty())
				{
					hasCore = false;
				}
				else if (!world.isRemote)
				{
					EntityItem ei = new EntityItem(world, pos.getX()+0.5, pos.getY()+1.5, pos.getZ()+0.5, new ItemStack(ItemRegistry.salvagePart, 1, 9));
					world.spawnEntity(ei);
					decrStackSize(0, 1);
				}
				world.notifyBlockUpdate(pos, world.getBlockState(pos), world.getBlockState(pos), 3);
			}
		}
	}

	@Override
	public int getField(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setField(int id, int value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getFieldCount() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		if (nbt.hasKey("core"))
		{
			hasCore = nbt.getBoolean("core");
		}
		if (nbt.hasKey("age"))
		{
			age = nbt.getInteger("age");
		}
	}
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setBoolean("core", hasCore);
		nbt.setInteger("age", age);
		return nbt;
	}
}