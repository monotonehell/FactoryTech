package dalapo.factech.tileentity.specialized;

import java.util.ArrayList;
import java.util.List;

import dalapo.factech.FactoryTech;
import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.FacMiscHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.init.ModFluidRegistry;
import dalapo.factech.reference.AABBList;
import dalapo.factech.reference.StateList;
import dalapo.factech.tileentity.IMagnifyingGlassInfo;
import dalapo.factech.tileentity.TileEntityBase;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ITickable;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;

public class TileEntityBlowtorch extends TileEntityBase implements ITickable, IMagnifyingGlassInfo
{
	private static final double k = 0.0025;
	
	private FluidTank tank;
	private EnumFacing direction;
	private double temperature = 10;
	private boolean hasLoaded = false;
	private boolean isFiring = false;
	
	public TileEntityBlowtorch()
	{
		tank = new FluidTank(10000) {
			public boolean canFillFluidType(FluidStack fluid)
		    {
		        return fluid != null && fluid.getFluid() == ModFluidRegistry.propane && canFill();
		    }
		};
		tank.setTileEntity(this);
	}
	
	private double adjustTemperature(double biomeTemp)
	{
		return (50*biomeTemp) - 7.5; // 0.15 = Max. temp for snow -> 0C. 0.95 = max temp for rain -> 40C
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		if (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY) return true;
		else return super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY) return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(tank);
		return super.getCapability(capability, facing);
	}
	
//	@Override
//	public void onLoad()
//	{
//		Logger.info("Entered onLoad");
//		temperature = adjustTemperature(world.getBiome(pos).getTemperature(pos));
//	}
	
	@Override
	public void update()
	{
		if (!hasLoaded) // Shouldn't the world finish loading the block *before* onLoad is called?
		{
			direction = world.getBlockState(pos).getValue(StateList.directions);
			hasLoaded = true;
		}
		
		updateTemperature();
		
		if (isPowered() && tank.getFluidAmount() > 0 && tank.getFluid().getFluid() == ModFluidRegistry.propane)
		{
			isFiring = true;
			if (!world.isRemote)
			{
				AxisAlignedBB attackBox = new AxisAlignedBB(FacMathHelper.withOffset(pos, direction));
				attackBox = attackBox.expand(3*direction.getFrontOffsetX(), 3*direction.getFrontOffsetY(), 3*direction.getFrontOffsetZ());
				for (EntityLivingBase e : world.getEntitiesWithinAABB(EntityLivingBase.class, attackBox))
				{
					e.setFire(5);
					e.attackEntityFrom(DamageSource.IN_FIRE, 5); // 2.5 hearts of fire da-ma-hey
				}
				
				if (temperature > 500)
				{
					for (EntityItem ei : world.getEntitiesWithinAABB(EntityItem.class, attackBox))
					{
						ItemStack is = FurnaceRecipes.instance().getSmeltingResult(ei.getItem()).copy();
						if (!is.isEmpty() && FactoryTech.random.nextInt((int)(17000 / temperature)) == 0)
						{
							EntityItem smelted = new EntityItem(world, ei.posX, ei.posY, ei.posZ, is);
							smelted.motionX = ei.motionX;
							smelted.motionY = ei.motionY;
							smelted.motionZ = ei.motionZ;
							world.spawnEntity(smelted);
							ei.getItem().shrink(1);
							if (ei.getItem().isEmpty()) ei.setDead();
						}
					}
					for (BlockPos bp : BlockPos.getAllInBox(pos, FacMathHelper.withOffsetAndDist(pos, direction, 4)))
					{
						IBlockState state = world.getBlockState(bp);
						if (!world.isAirBlock(bp) && FactoryTech.random.nextInt(20) == 0)
						{
							Block b = state.getBlock();
							ItemStack is = FurnaceRecipes.instance().getSmeltingResult(new ItemStack(b)).copy();
							if (!is.isEmpty())
							{
								if (is.getItem() instanceof ItemBlock)
								{
									IBlockState targetState = ((ItemBlock)is.getItem()).getBlock().getStateFromMeta(is.getItemDamage());
									world.setBlockState(bp, targetState);
								}
								else
								{
									world.setBlockToAir(bp);
									EntityItem ei = new EntityItem(world, bp.getX()+0.5, bp.getY()+0.5, bp.getZ()+0.5, is);
									world.spawnEntity(ei);
								}
							}
							else if (state.getMaterial().getCanBurn() && FactoryTech.random.nextInt(20) == 0)
							{
								world.spawnParticle(EnumParticleTypes.SMOKE_LARGE, bp.getX()+0.5, bp.getY()+0.5, bp.getZ()+0.5, 0, 0, 0);
								world.playSound(null, bp, SoundEvents.ENTITY_GENERIC_EXTINGUISH_FIRE, SoundCategory.BLOCKS, 1, 1);
								world.setBlockToAir(bp);
							}
						}
						
					}
				}
			}
			Vec3d dirVec = new Vec3d(direction.getDirectionVec()).scale(0.25);
			Vec3d vf = FacMathHelper.randomOffset(dirVec, 0.1, 0.1, 0.1);
			for (int i=0; i<5; i++) world.spawnParticle(EnumParticleTypes.FLAME, pos.getX()+0.5+(direction.getFrontOffsetX()/2.0), pos.getY()+0.5+(direction.getFrontOffsetY()/2.0), (pos.getZ()+0.5+direction.getFrontOffsetZ()/2.0), vf.x, vf.y, vf.z);
			if (temperature > 900 && FactoryTech.random.nextInt(3) == 0)
			{
				world.spawnParticle(EnumParticleTypes.LAVA, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, 0, 0, 0);
			}
			tank.drainInternal(3, true);
		}
		else
		{
			isFiring = false;
		}
	}
	
	private void updateTemperature()
	{
		double ambientTemp = (isFiring ? 1000.0 : adjustTemperature(world.getBiome(pos).getTemperature(pos)));
		temperature += (ambientTemp - temperature) * k;
		if (temperature > 900.0)
		{
			world.setBlockToAir(pos);
			world.createExplosion(null, pos.getX(), pos.getY(), pos.getZ(), 4, true);
		}
	}
	
	@Override
	public void markDirty()
	{
		super.markDirty();
		hasLoaded = false;
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		tank.writeToNBT(nbt);
		nbt.setDouble("temp", temperature);
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
//		Logger.debug("Entered readFromNBT");
		super.readFromNBT(nbt);
		tank.readFromNBT(nbt);
		temperature = nbt.getDouble("temp");
	}

	@Override
	public void showChatInfo(EntityPlayer ep) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public List<String> getGlassInfo() {
		List<String> info = new ArrayList<String>();
		info.add(FacMiscHelper.describeTank(tank));
		info.add(I18n.format("factorytech.temperature", (int)temperature));
		return info;
	}
}