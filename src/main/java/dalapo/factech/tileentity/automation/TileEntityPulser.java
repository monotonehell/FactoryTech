package dalapo.factech.tileentity.automation;

import dalapo.factech.helper.FacBlockHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.reference.StateList;
import dalapo.factech.tileentity.ActionOnRedstone;
import dalapo.factech.tileentity.TileEntityBase;
import dalapo.factech.tileentity.TileEntityBasicInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;

public class TileEntityPulser extends TileEntityBase implements ActionOnRedstone, ITickable
{

	// Changed constantly while TE ticks
	private boolean isPulsing;
	private boolean isPowered;
	private int remainingPulses;
	private int remainingWait;
	private boolean currentState;
	
	// Only changed when set by GUI
	private int ticksPer;
	private int ticksBetween;
	private int numPulses;
	
	public TileEntityPulser()
	{
		ticksPer = 1;
		ticksBetween = 1;
		numPulses = 1;
	}
	
	@Override
	public void onRedstoneSignal(boolean isSignal, EnumFacing side)
	{
		if (world.isBlockPowered(pos) && side != world.getBlockState(pos).getValue(StateList.directions) && !isPulsing)
		{
			if (!isPowered && isSignal)
			{
				beginPulseSequence();
				isPowered = true;
			}
		}
		else if (!isSignal) isPowered = false;
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		world.setBlockState(pos, world.getBlockState(pos).withProperty(StateList.powered, currentState));
	}
	
	@Override
	public void update()
	{
		if (isPulsing)
		{
			if (remainingWait-- == 0)
			{
				remainingWait = 2 * togglePulse(); // 1 Redstone tick = 2 game ticks

				world.setBlockState(pos, world.getBlockState(pos).withProperty(StateList.powered, currentState));
				FacBlockHelper.updateBlock(world, pos);
			}
			if (remainingPulses == 0) isPulsing = false;
		}
	}
	
	private int togglePulse()
	{
		if (currentState) remainingPulses--;
		currentState = !currentState;
		return currentState ? ticksPer : ticksBetween;
	}
	
	public int getOutputPower()
	{
		return currentState ? 15 : 0;
	}
	
	private void beginPulseSequence()
	{
		remainingPulses = numPulses;
		isPulsing = true;
	}
	
	public boolean getIsPowered()
	{
		return isPowered;
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setBoolean("isPulsing", isPulsing);
		nbt.setBoolean("isPowered", isPowered);
		nbt.setInteger("remainingPulses", remainingPulses);
		nbt.setInteger("remainingWait", remainingWait);
		nbt.setBoolean("currentState", currentState);
		nbt.setInteger("ticksOn", ticksPer);
		nbt.setInteger("ticksOff", ticksBetween);
		nbt.setInteger("numPulses", numPulses);
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		isPulsing = nbt.getBoolean("isPulsing");
		isPowered = nbt.getBoolean("isPowered");
		remainingPulses = nbt.getInteger("remainingPulses");
		remainingWait = nbt.getInteger("remainingWait");
		currentState = nbt.getBoolean("currentState");
		ticksPer = nbt.getInteger("ticksOn");
		ticksBetween = nbt.getInteger("ticksOff");
		numPulses = nbt.getInteger("numPulses");
	}
	
	@Override
	public int getField(int id)
	{
		switch(id)
		{
		case 0:
			return numPulses;
		case 1:
			return ticksPer;
		case 2:
			return ticksBetween;
			default:
				return 0;
		}
	}
	
	@Override
	public void setField(int id, int val)
	{
		switch(id)
		{
		case 0:
			numPulses = val;
			break;
		case 1:
			ticksPer = val;
			break;
		case 2:
			ticksBetween = val;
			break;
		}
		markDirty();
	}
	
	@Override
	public int getFieldCount()
	{
		return 3;
	}
}