package dalapo.factech.tileentity.automation;

import java.util.ArrayList;
import java.util.List;

import dalapo.factech.auxiliary.ChunkLoadRegistry;
import dalapo.factech.auxiliary.ChunkWithDimension;
import dalapo.factech.auxiliary.IChunkLoader;
import dalapo.factech.auxiliary.PosWithDimension;
import dalapo.factech.config.FacTechConfigManager;
import dalapo.factech.helper.FacBlockHelper;
import dalapo.factech.helper.FacChatHelper;
import dalapo.factech.helper.FacEntityHelper;
import dalapo.factech.helper.FacTileHelper;
import dalapo.factech.helper.Logger;
import dalapo.factech.packet.PacketHandler;
import dalapo.factech.packet.PlayerChatPacket;
import dalapo.factech.tileentity.ActionOnRedstone;
import dalapo.factech.tileentity.IMagnifyingGlassInfo;
import dalapo.factech.tileentity.TileEntityBasicInventory;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ITickable;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;

public class TileEntityPlaneShifter extends TileEntityBasicInventory implements IMagnifyingGlassInfo, ISidedInventory, IChunkLoader, ITickable, ActionOnRedstone
{
	private static final boolean actuallyLoadChunks = true;
	private int dimensionID;
	private int chorusStorage;
	private boolean loaded;
	private boolean isPowered;
	private List<ChunkWithDimension> cache = new ArrayList<>();
	private PosWithDimension dimPos;
	
	/**
	 * Used for the TESR.
	 */
	@SideOnly(Side.CLIENT)
	public float ticks;
	public static final float dt = 0.1F;
	
	public TileEntityPlaneShifter()
	{
		super("planeshifter", 10);
		setDisplayName("Dimensional Shifter");
	}
	
	public void shiftItems()
	{
		World dimension = world.getMinecraftServer().getWorld(dimensionID);
		int acc = 0;
		for (int i=0; i<9; i++)
		{
			acc += getStackInSlot(i).getCount();
		}
		int chorusRequired = (int)Math.ceil((double)acc / FacTechConfigManager.itemsPerFruit);
		chorusStorage -= Math.min(chorusStorage, chorusRequired);
		if (chorusStorage >= chorusRequired)
		{
			for (int i=0; i<9; i++)
			{
				ItemStack is = getStackInSlot(i);
				EntityItem ei = new EntityItem(dimension, dimPos.getX()+0.5, dimPos.getX()+0.5, dimPos.getZ()+0.5, is);
				FacEntityHelper.stopEntity(ei);
				dimension.spawnEntity(ei);
				setInventorySlotContents(i, ItemStack.EMPTY);
			}
			world.playSound(null, pos, SoundEvents.ENTITY_ENDERMEN_TELEPORT, SoundCategory.BLOCKS, 1, 1);
		}
		else
		{
			world.spawnParticle(EnumParticleTypes.SMOKE_LARGE, pos.getX()+0.5, pos.getY()+1, pos.getZ()+0.5, 0, 0, 0);
			world.playSound(null, pos, SoundEvents.ENTITY_GENERIC_EXTINGUISH_FIRE, SoundCategory.BLOCKS, 1, 1);
		}
		FacBlockHelper.updateBlock(world, pos);
	}

	private void getDimensionPos()
	{
		if (world instanceof WorldServer)
		{
			double ratio = world.provider.getMovementFactor() / world.getMinecraftServer().getWorld(dimensionID).provider.getMovementFactor();
			dimPos =  new PosWithDimension(world.getMinecraftServer().getWorld(dimensionID), dimensionID, pos.getX()*ratio, pos.getY(), pos.getZ()*ratio);
		}
	}
	
	@Override
	public void onInventoryChanged(int slot)
	{
		if (slot == 9 && getStackInSlot(9).getItem() == Items.CHORUS_FRUIT_POPPED)
		{
			int toFill = Math.min(getStackInSlot(9).getCount(), 256 - chorusStorage);
			chorusStorage += toFill;
			getStackInSlot(9).shrink(toFill);
		}
		FacBlockHelper.updateBlock(world, pos);
	}
	
	@Override
	public void update()
	{
		if (world.isRemote) ticks += dt; 
		if (actuallyLoadChunks && !loaded && !world.isRemote)
		{
			loaded = true;
			getDimensionPos();
			ChunkLoadRegistry.instance.loadChunks(dimPos, getChunksToLoad());
		}
	}
	
	@Override
	public void invalidate()
	{
		ChunkLoadRegistry.instance.unloadChunks(dimPos);
	}
	
	public void changeDimension(int newDim)
	{
		ChunkLoadRegistry.instance.unloadChunks(dimPos);
		dimensionID = newDim;
		getDimensionPos();
		loaded = false;
	}
	
//	@Override
//	public void invalidate()
//	{
//		if (!world.isRemote) ChunkLoadRegistry.instance.unloadChunks(new PosWithDimension(this));
//	}

	@Override
	public List<ChunkWithDimension> getChunksToLoad()
	{
		if (dimPos == null) return new ArrayList<>();
		if (cache.isEmpty())
		{
			double thisMovement = world.provider.getMovementFactor();
			double otherMovement = world.getMinecraftServer().getWorld(dimensionID).provider.getMovementFactor();
			double factor = thisMovement / otherMovement;
			Chunk chunk = world.getChunkFromBlockCoords(dimPos);
			cache.add(new ChunkWithDimension(dimensionID, chunk.x, chunk.z));
		}
		return cache;
	}

	public int getDimension()
	{
		return dimensionID;
	}

	@Override
	public void onRedstoneSignal(boolean isSignal, EnumFacing side)
	{
		if (world.isBlockPowered(pos))
		{
			if (!isPowered && isSignal)
			{
				isPowered = true;
				shiftItems();
			}
		}
		else if (!isSignal) isPowered = false;
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setInteger("chorus", chorusStorage);
		nbt.setInteger("dimension", dimensionID);
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		chorusStorage = nbt.getInteger("chorus");
		dimensionID = nbt.getInteger("dimension");
	}

	@Override
	public int[] getSlotsForFace(EnumFacing side) {
		if (side == EnumFacing.DOWN) return new int[] {9};
		else return new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8};
	}

	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction)
	{
		return (direction != EnumFacing.DOWN || (index == 9 && itemStackIn.getItem() == Items.CHORUS_FRUIT_POPPED));
	}

	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public int getField(int id)
	{
		if (id == 0) return chorusStorage;
		if (id == 1) return dimensionID;
		return 0;
	}
	
	@Override
	public void setField(int id, int val)
	{
		if (id == 0) chorusStorage = val;
		if (id == 1) dimensionID = val;
	}
	
	@Override
	public int getMaxField(int id)
	{
		if (id == 0) return 256;
		return Integer.MAX_VALUE;
	}

	public PosWithDimension getDimPos()
	{
		return dimPos;
	}
	
	@Override
	public void showChatInfo(EntityPlayer ep)
	{
		FacChatHelper.sendChatToPlayer(ep, I18n.format("factorytech.planeshifter.attuned", dimensionID));
	}
	
	@Override
	public void showChatInfoServerSide(EntityPlayer ep)
	{
		PacketHandler.sendToPlayer(new PlayerChatPacket(I18n.format("factorytech.planeshifter.location", dimPos.getX(), dimPos.getY(), dimPos.getZ())), ep);
	}

	@Override
	public List<String> getGlassInfo() {
		// TODO Auto-generated method stub
		List<String> info = new ArrayList<String>();
		info.add("Attuned dimension: " + dimensionID);
		return info;
	}
}