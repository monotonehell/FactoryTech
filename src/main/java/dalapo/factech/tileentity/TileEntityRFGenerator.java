package dalapo.factech.tileentity;

import dalapo.factech.helper.FacMathHelper;
import dalapo.factech.helper.FacStackHelper;
import dalapo.factech.helper.Logger;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.CapabilityItemHandler;

/**
 * Implementation of a basic RF generator.
 * @author Vazkii, mostly
 */
public abstract class TileEntityRFGenerator extends TileEntityBasicInventory implements ITickable, ISidedInventory
{
	private int internalStorage;
	
	public IEnergyStorage buffer = new IEnergyStorage() {

		@Override
		public int receiveEnergy(int maxReceive, boolean simulate) {return 0;}
		@Override
		public int extractEnergy(int maxExtract, boolean simulate) {return 0;}
		@Override
		public int getEnergyStored() {return internalStorage;}
		@Override
		public int getMaxEnergyStored() {return getMaxStorage();}
		@Override
		public boolean canExtract() {return false;}
		@Override
		public boolean canReceive() {return false;}
	};
	
	public TileEntityRFGenerator(String name, int slots)
	{
		super(name, slots);
	}
	
	protected boolean doOutput(ItemStack out, int outSlot)
	{
		if (getStackInSlot(outSlot).isEmpty())
		{
			setInventorySlotContents(outSlot, out);
			return true;
		}
		if (FacStackHelper.canCombineStacks(getStackInSlot(outSlot), out))
		{
			getStackInSlot(outSlot).grow(out.getCount());
			return true;
		}
		return false;
	}
	
	protected void feedStorage(int amount)
	{
		internalStorage += Math.min(amount, getMaxStorage() - internalStorage);
	}

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing side)
	{
		if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || capability == CapabilityEnergy.ENERGY) return true;
		return super.hasCapability(capability, side);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing side)
	{
		if (capability == CapabilityEnergy.ENERGY) return CapabilityEnergy.ENERGY.cast(buffer);
		else if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) return super.getCapability(capability, side);
		return null;
	}

	@Override
	public void update()
	{
		int toTransfer = Math.min(getMaxTransfer(), internalStorage);
		internalStorage -= toTransfer;
		internalStorage += transferNRG(toTransfer);
	}
	
	protected abstract int getMaxTransfer();
	public abstract int getMaxStorage();
	
	private int transferNRG(int nrg)
	{
		for (EnumFacing side : getOutputSides())
		{
			BlockPos neighbour = FacMathHelper.withOffset(pos, side);
			if (!world.isBlockLoaded(FacMathHelper.withOffset(pos, side))) continue;
			TileEntity te = world.getTileEntity(neighbour);
			
			if (te != null)
			{
				IEnergyStorage storage = null;
				if (te.hasCapability(CapabilityEnergy.ENERGY, side.getOpposite()))
					storage = te.getCapability(CapabilityEnergy.ENERGY, side.getOpposite());
				else if (te.hasCapability(CapabilityEnergy.ENERGY, null))
					storage = te.getCapability(CapabilityEnergy.ENERGY, null);
				
				if (storage != null)
				{
//					Logger.info("Sending " + nrg + " RF to TE at pos " + neighbour);
					nrg -= storage.receiveEnergy(nrg, false);
					if (nrg <= 0) return 0;
				}
			}
		}
		return nrg;
	}
	
	protected EnumFacing[] getOutputSides()
	{
		return EnumFacing.VALUES;
	}
	

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setInteger("nrg", internalStorage);
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		internalStorage = nbt.getInteger("nrg");
	}
	
	@Override
	public int getField(int id)
	{
		if (id == 0) return internalStorage;
		return 0;
	}
	
	@Override
	public int getMaxField(int id)
	{
		if (id == 0) return buffer.getMaxEnergyStored();
		return 0;
	}
	
	@Override
	public int[] getSlotsForFace(EnumFacing side) {
		// TODO Auto-generated method stub
		return new int[] {0, 1};
	}

	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction) {
		// TODO Auto-generated method stub
		return index == 0;
	}

	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
		// TODO Auto-generated method stub
		return index == 1;
	}
}