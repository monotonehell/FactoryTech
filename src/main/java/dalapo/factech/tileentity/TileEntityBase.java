package dalapo.factech.tileentity;

import static dalapo.factech.FactoryTech.DEBUG_PACKETS;

import javax.annotation.Nullable;

import dalapo.factech.block.BlockDirectional;
import dalapo.factech.helper.Logger;
import dalapo.factech.reference.StateList;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

public abstract class TileEntityBase extends TileEntity {
	@Nullable protected EnumFacing direction;
//	public abstract void sendInfoPacket(EntityPlayer ep);
	
	public void setDirection(EnumFacing dir)
	{
		this.direction = dir;
	}
	
	public EnumFacing getDirection() // direction may be null
	{
		return direction;
	}
	
	public boolean isPowered()
	{
		return world.isBlockIndirectlyGettingPowered(pos) > 0;
	}
	
	protected void markDirtyLight()
	{
		if (world != null)
		{
			world.markChunkDirty(pos, this);
		}
	}
	
	@Override
	public NBTTagCompound getUpdateTag()
	{
		if (DEBUG_PACKETS)
		{
			Logger.info("Entered getUpdateTag()");
			Thread.dumpStack();
		}
		NBTTagCompound nbt = super.getUpdateTag();
		return writeToNBT(nbt);
	}
	
	@Override
	public SPacketUpdateTileEntity getUpdatePacket()
	{
		if (DEBUG_PACKETS)
		{
			Logger.info(String.format("Entered getUpdatePacket; thread = %s", Thread.currentThread()));
			Thread.dumpStack();
		}
		NBTTagCompound nbt = new NBTTagCompound();
		this.writeToNBT(nbt);
		return new SPacketUpdateTileEntity(getPos(), 1, nbt);
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity packet)
	{
//		Logger.info(String.format("Entered onDataPacket; thread = %s", Thread.currentThread()));
		this.readFromNBT(packet.getNbtCompound());
	}
	
	@Override
	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newState)
	{
		return oldState.getBlock() != newState.getBlock();
	}
	
	public int getField(int id) { return 0; }
	public void setField(int id, int val) {}
	public int getFieldCount() { return 0; }
	public int getMaxField(int id) { return 0; }
	
	public void toggleField(int id)
	{
		if (getField(id) == 0) setField(id, 1);
		else setField(id, 0);
	}

	public boolean isUsableByPlayer(EntityPlayer ep)
	{
		return true;
	}
	
	public void onNeighbourChange(BlockPos changedPos)
	{
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		if (direction != null) nbt.setInteger("direction", direction.getIndex());
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		if (nbt.hasKey("direction")) direction = EnumFacing.getFront(nbt.getInteger("direction"));
		else direction = null;
	}
}