package dalapo.factech.tileentity;

import java.util.List;

import dalapo.factech.helper.Pair;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import static dalapo.factech.helper.FacRenderHelper.Point;

public interface IMagnifyingGlassInfo
{
	public void showChatInfo(EntityPlayer ep);
	public List<String> getGlassInfo(); // 1 String per line
	public default void showChatInfoServerSide(EntityPlayer ep) {}
}